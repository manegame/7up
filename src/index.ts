const { exec } = require('child_process')

import {
  QMainWindow,
  QWidget,
  QLabel,
  FlexLayout,
  QPushButton,
  QPixmap,
  WidgetEventTypes,
  QDragMoveEvent,
  QDragLeaveEvent,
  QDropEvent,
  NativeElement
} from '@nodegui/nodegui';

// # # # # # # #

// CONFIG

// # # # # # # #

let spriteUrls = new Array()

// GENERAL LAYOUT

const win = new QMainWindow();
win.setWindowTitle("Spritesheet generator");

const centralWidget = new QWidget();
centralWidget.setObjectName("myroot");

// Top
const rootLayout = new FlexLayout();
centralWidget.setLayout(rootLayout);

const generateButton = new QPushButton()
generateButton.setText('Generate Spritesheet')
generateButton.addEventListener('clicked', () => {
  const urls = spriteUrls.map((url) => {
    return '"' + url + '"'
  })
  const filesString = urls.join(' ')
  console.log(filesString)

  exec(`spritesheet-js -p ~/Desktop ${filesString}`, (error:any, stdout:any, stderr:any) => {
    if (error) {
      console.error(`exec error: ${error}`);
      return;
    }
    if (stdout) {
      const success = new QLabel()
      success.setText('Success')
      rootLayout.addWidget(success)
    }
    console.log(`stdout: ${stdout}`);
    console.error(`stderr: ${stderr}`);
  })
})
rootLayout.addWidget(generateButton)

// Drop images
const dropZone = new QWidget();
dropZone.setObjectName('dropZone')
dropZone.setAcceptDrops(true);

// Show added images
const listZoneLayout = new FlexLayout()
const listZone = new QWidget();
listZone.setObjectName('listZone')
listZone.setLayout(listZoneLayout)

// Again
const againButton = new QPushButton()
againButton.setText('Again')
againButton.addEventListener('clicked', () => {
  spriteUrls.forEach((url) => {
    listZoneLayout.removeWidget(url)
  })
  spriteUrls = new Array()
})
rootLayout.addWidget(againButton)

// # # # # # # # 

// EVENTS

// # # # # # # #

// DRAG ENTER EVENT
dropZone.addEventListener(WidgetEventTypes.DragEnter, (e) => {
  e = e as NativeElement
  let ev = new QDragMoveEvent(e);
  console.log('dragEnter', ev.proposedAction());
  let mimeData = ev.mimeData();
  mimeData.text(); //Inspection of text works
  console.log('mimeData', {
      hasColor: mimeData.hasColor(),
      hasHtml: mimeData.hasHtml(),
      hasImage: mimeData.hasImage(),
      hasText: mimeData.hasText(),
      hasUrls: mimeData.hasUrls(),
      html: mimeData.html(),
      text: mimeData.text(),
  }); //Inspection of MIME data works
  // let urls = mimeData.urls(); //Get QUrls
  // for (let url of urls) {
  //     // let str = url.toString();
  // }
  ev.accept(); //Accept the drop event, which is crucial for accepting further events
});

// DRAG MOVE EVENT
dropZone.addEventListener(WidgetEventTypes.DragMove, (e) => {
  e = e as NativeElement
  let ev = new QDragMoveEvent(e);

  console.log('dragMove');
});

// DRAG LEAVE EVENT
dropZone.addEventListener(WidgetEventTypes.DragLeave, (e) => {
  e = e as NativeElement
  console.log('dragLeave', e);
  let ev = new QDragLeaveEvent(e);
  ev.ignore(); //Ignore the event when it leaves
  console.log('ignored', ev);
});

// DROP EVENT
dropZone.addEventListener(WidgetEventTypes.Drop, (e) => {
  e = e as NativeElement
  let dropEvent = new QDropEvent(e);
  let mimeData = dropEvent.mimeData();
  console.log('dropped', dropEvent.type());
  let urls = mimeData.urls();
  for (let url of urls) {
      let str = url.toString();
      str = str.replace('file://', '')

      if (!spriteUrls.includes(str)) {
        const imageContainer = new QLabel()
        const image = new QPixmap()
        const loaded = image.load(str)

        if (loaded) {
          imageContainer.setPixmap(image)
          imageContainer.setObjectName(str)
  
          spriteUrls.push(str)
  
          listZoneLayout.addWidget(imageContainer)
        }
      }
  }
});

// # # # # # # # 

// INIT

// # # # # # # #

rootLayout.addWidget(dropZone);
rootLayout.addWidget(listZone);
win.setCentralWidget(centralWidget);
win.setStyleSheet(
  `
    #myroot {
      background-color: #fff;
      height: '100%';
      align-items: 'flex-start';
      justify-content: 'flex-start';
    }
    #dropZone {
      background-color: #ccc;
      min-height: 120px;
      min-width: 480px;
    }
    #listZone {
      background-color: #fff;
      min-height: 360px;
      min-width: 480px;
      flex-flow: row wrap;
    }
    #listZone QLabel {
      max-width: 120px;
      max-height: 120px;
      flex-shrink: 1;
    }
  `
);
win.show();

(global as any).win = win;
